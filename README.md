# API-MOVIES

Este proyecto contiene código fuente y archivos de soporte para una aplicación sin servidor que puede implementar con la CLI de SAM. Incluye los siguientes archivos y carpetas.

- movies - Código para la función Lambda de la aplicación.
- movies/script.sql - Archivo con sentencias sql para generar tabla y bd.
- template.yaml - Una plantilla que define los recursos de AWS de la aplicación.

La aplicación utiliza varios recursos de AWS, incluidas las funciones de APILambda y una API Gateway. Estos recursos se definen en el archivo `template.yaml` en este proyecto. Puede actualizar la plantilla para agregar recursos de AWS a través del mismo proceso de implementación que actualiza el código de su aplicación.

## Requisitos e Instalación 🔧

La interfaz de línea de comandos del modelo de aplicación sin servidor (SAM CLI) es una extensión de la AWS CLI que agrega funcionalidad para crear y probar aplicaciones Lambda. Utiliza Docker para ejecutar sus funciones en un entorno de Amazon Linux que coincida con Lambda. También puede emular el entorno de compilación y la API de su aplicación.

Para usar la CLI de SAM, necesita las siguientes herramientas.

- SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- Node.js - [Install Node.js 10](https://nodejs.org/en/), including the NPM package management tool.
- Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community)

Ahora que tienes lo necesario , necesitas hacer el clon del proyecto :

```
https://gitlab.com/diegoescaff/api-sam-movies.git
```

lo primero es ingresar a la carpeta `/movies` y ejecutar el siguiente comando :

```
npm install
```

Lo siguiente es ejecutar los script del archivo `script.sql` estos deben ser ejecutados en un base de datos MySql, posterior a esto debemos configurar los datos de nuestra coneccion desde el archivo `db-config.js`

Congigurar host de la Base de datos

```bash
    host = 'localhost'
```

Congigurar parametros

```bash
    user: 'root',
    password: '123456',
    database: 'movies',
```

## Despliegue 📦

## Use la CLI de SAM para construir y probar localmente

Cree su aplicación con el `sam build` comando.

```bash
sam build
```

La CLI de SAM instala las dependencias definidas en `movies/package.json`, crea un paquete de implementación y lo guarda en la carpeta`.aws-sam/build` .

Pruebe una sola función invocando directamente con un evento de prueba. Un evento es un documento JSON que representa la entrada que recibe la función del origen del evento. Los eventos de prueba se incluyen en la carpeta `events` de este proyecto.

Ejecute funciones localmente e invoque con el comnado `sam local invoke` .

```bash
 sam local invoke
```

La CLI de SAM también puede emular la API de su aplicación. Utilizar el`sam local start-api` para ejecutar la API localmente en el puerto 3000.

```bash
 sam local start-api

```

- http://127.0.0.1:3000/movie/list [get] - Obtiene todas las peliculas en formato Json
- http://127.0.0.1:3000/movie [post]- Inserta una nueva pelicula
- http://127.0.0.1:3000/movie [put]- Actualizar una pelicula
- http://127.0.0.1:3000/movie/{id} [delete]- Eliminar una pelicula de la BD

Ejemplo Json POST

```json
{
  "Title": "Spiderman 4",
  "Year": 2020,
  "Age": "19",
  "IMDb": 4.7,
  "Rotten": "80",
  "Netflix": 0,
  "Hulu": 0,
  "Prime": 1,
  "Type": 0,
  "Disney": 1,
  "Directors": "nolan",
  "Genres": "Comic",
  "Country": "USA",
  "Languaje": "español",
  "RunTime": 100
}
```

Ejemplo Json PUT

```json
{
  "id": 1,
  "Title": "Spiderman 4",
  "Year": 2020,
  "Age": "19",
  "IMDb": 4.7,
  "Rotten": "80",
  "Netflix": 0,
  "Hulu": 0,
  "Prime": 1,
  "Type": 0,
  "Disney": 1,
  "Directors": "nolan",
  "Genres": "Comic",
  "Country": "USA",
  "Languaje": "español",
  "RunTime": 100
}
```
