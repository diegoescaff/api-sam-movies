// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */



const dbConfig = require('./db-config');

const mysql = require('serverless-mysql')({
    config: dbConfig

});





exports.lambdaHandler = async(event, context) => {
    try {
        // const ret = await axios(url);
        response = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Api Movies',
                endpoints: {
                    movielist: '[get]/movie/list',
                    moviepost: '[post]/movie',
                    movieupdate: '[put]/movie',
                    moviedelete: '[delete]/movie/{id}'
                }
                // location: ret.data.trim()
            })
        }
    } catch (err) {
        console.log(err);
        return err;
    }

    return response
};


exports.getall = async(event, context) => {
    try {

        const functions = await mysql.query('SELECT id , Title, Year, Age, IMDb,Rotten ,Netflix, Hulu, Prime FROM movies order by Title');
        await mysql.end();


        response = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Lista de peliculas',
                items: functions
                    // location: ret.data.trim()
            })
        }

        return response
    } catch (err) {
        console.log('error', err);
        return err;
    }


};


exports.post = async(event, context) => {
    const data = JSON.parse(event.body);
    const { Title, Year, Age, IMDb, Rotten, Netflix, Hulu, Prime, Type, Disney, Directors, Genres, Country, Languaje, RunTime } = data;

    try {

        const query = `INSERT INTO movies (id, Title, Year, Age, IMDb, Rotten, Netflix, Hulu, Prime, Type, Disney, Directors, Genres, Country, Languaje, RunTime) VALUES(null,'${Title}', ${Year}, '${Age}+', ${IMDb}, '${Rotten}%', ${Netflix}, ${Hulu}, ${Prime}, ${Type}, ${Disney}, '${Directors}', '${Genres}', '${Country}', '${Languaje}', ${RunTime});`;

        const functions = await mysql.query(query);
        await mysql.end();

        // const ret = await axios(url);
        response = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Pelicula insertada',
                // location: ret.data.trim()
            })
        }
        return response
    } catch (err) {
        console.log(err);
        return err;
    }


};


exports.put = async(event, context) => {
    const data = JSON.parse(event.body);
    try {

        const { id, Title, Year, Age, IMDb, Rotten, Netflix, Hulu, Prime, Type, Disney, Directors, Genres, Country, Languaje, RunTime } = data;

        const query = `UPDATE movies SET Title='${Title}', Year=${Year}, Age='${Age}+', IMDb=${IMDb}, Rotten='${Rotten}', Netflix=${Netflix}, Hulu=${Hulu}, Prime=${Prime}, Type=${Type}, Disney=${Disney}, Directors='${Directors}', Genres='${Genres}', Country='${Country}', Languaje='${Languaje}', RunTime=${RunTime} WHERE id=${id}; `

        const functions = await mysql.query(query);

        await mysql.end();


        // const ret = await axios(url);
        response = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Pelicula actualizada',
                // location: ret.data.trim()
            })
        }
        return response
    } catch (err) {
        console.log(err);
        return err;
    }
};


exports.delete = async(event, context) => {
    const params = { id: event.pathParameters.id };

    try {
        const { id } = params;
        if (id == null || id == undefined) {
            let error = new Error();
            error.message = 'El id es requerido'
            error.name = 'Error al eliminar la pelicula'
            throw error;
        }
        const query = `DELETE FROM movies WHERE id=${id};`;

        const functions = await mysql.query(query);
        await mysql.end();

        // const ret = await axios(url);
        response = {
            statusCode: 200,
            body: JSON.stringify({
                message: 'hello delete',
                // location: ret.data.trim()
            })
        }
        return response
    } catch (err) {
        console.log(err);
        return err;
    }


};