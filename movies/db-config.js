const host = 'localhost';
const dbHost = process.env.AWS_SAM_LOCAL === 'true' ? 'host.docker.internal' : host;

module.exports = {
    host: dbHost,
    user: 'root',
    password: '123456',
    database: 'movies',
};