###CREAR BD###
CREATE DATABASE movies

USE movies

###CREAR TABLA MOVIES #####
CREATE TABLE movies (
  id int(6) NOT NULL AUTO_INCREMENT,
  Title varchar(100) NOT NULL,
  Year int(11) NOT NULL,
  Age varchar(4) DEFAULT NULL,
  IMDb decimal(5,1) NOT NULL,
  Rotten varchar(4) DEFAULT NULL,
  Netflix tinyint(4) NOT NULL,
  Hulu tinyint(4) NOT NULL,
  Prime tinyint(4) NOT NULL,
  Type tinyint(4) NOT NULL,
  Disney tinyint(4) NOT NULL,
  Directors varchar(100) DEFAULT NULL,
  Genres varchar(100) NOT NULL,
  Country varchar(100) NOT NULL,
  Languaje varchar(150) DEFAULT NULL,
  RunTime tinyint(4) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


###INSERTAR DATOS PRUEBA#####
INSERT INTO movies VALUES(null,'Batman', 2005, '15+', 5.5, '88%', 0 , 0 , 1 , 0, 1, 'Nolan', 'Comics', 'USA', 'Español', 100);
INSERT INTO movies VALUES(null,'Batman Vs Superman', 2015, '15+', 5.5, '80%', 0 , 0 , 1 , 0, 1, 'Nolan', 'Comics', 'USA', 'Español', 100);
INSERT INTO movies VALUES(null,'Batman Ark', 2008, '15+', 7.5, '90%', 0 , 0 , 1 , 0, 1, 'Nolan', 'Comics', 'USA', 'Español', 100);
